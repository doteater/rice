This is my collection of pretty terminal rice.

Here's a description of each script along with where I found it:

	256color.pl - Perl color test script from https://github.com/janbrennen/rice

	quinemas2010.rb - Beautiful ruby quine from http://mamememo.blogspot.com/2010/12/merry-quine-mas-2010.html. Prints its own source code with an animation. 

	snowjob.sh - Snow! from https://gist.github.com/sontek/1505483

	snowjob.py - Python version, https://gist.github.com/sontek/1508912

-pipes.sh variants - various versions of the popular terminal pipes screen saver.

	pipes.sh.0 - had to change the first line from /bin/sh to /bin/bash to make it work

	pipes.sh.1 - pimped out version from https://gist.github.com/livibetter/4689307

	pipes.sh.2 - version from janbrennen's rice collection: https://github.com/janbrennen/rice

-Bash art from http://pastebin.com/u/toruokada, video game characters and more

	bashcontra - Creepy monster face, its the boss of SuperC stage 6. 

	bashgremlin - One of the title characters from the movie Gremlins.

	bashkinghippo - One of the contenders from Punch-Out!

	bashkirby - It's Kirby!

	bashmotherbrain - Mother Brain, the final boss of Metroid.

	bashmushies1.1 - Various colored mushrooms!

	bashrufus - A big red robot (?) with a skull on his chest. I don't know what this guy is from.

-"Hacking" - phony scary "haxor" stuff prints in the terminal - mostly from https://github.com/janbrennen/rice

	3spooky.lua - Movie hacker "skull" interface, animated, eyes move around. I need to run it like "clear; lua 3spooky.lua"
	
	hack.exe - An almost believe one. Run with the super-secure domain you want to "hack" IE "./hack.exe whitehouse.gov"

        hackertyper.sh - Types out a bunch of complicated-looking C code when you randomly pound the keys culminating in a "hack". -h for options and info.

	

		
	

	

	




